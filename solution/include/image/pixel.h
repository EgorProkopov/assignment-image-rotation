//
// Created by ezh on 30/10/22.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_PIXEL_H
#define ASSIGNMENT_IMAGE_ROTATION_PIXEL_H

#include <stdint.h>

#define PIXEL_SIZE sizeof(struct pixel)
#define PIXEL_CHANNELS 3

struct pixel{
    uint8_t b;
    uint8_t g;
    uint8_t r;
};

#endif //ASSIGNMENT_IMAGE_ROTATION_PIXEL_H
