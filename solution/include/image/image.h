//
// Created by ezh on 30/10/22.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <stdint.h>
#include <stdio.h>

struct image{
    size_t width;
    size_t height;
    struct pixel* data;
};

//image functions
void set_pixel(struct image* const image, struct pixel const pixel, size_t x, size_t y);

struct pixel get_pixel(struct image const* image, size_t x, size_t y);

void init_image(struct image* image, FILE* const in, size_t const height, size_t const width);

void free_image(struct image* image);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
