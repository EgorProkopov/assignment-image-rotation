//
// Created by ezh on 30/10/22.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_PRINT_H
#define ASSIGNMENT_IMAGE_ROTATION_PRINT_H

void print_message(const char* message);
void print_error_message(const char* error_message);

#endif //ASSIGNMENT_IMAGE_ROTATION_PRINT_H
