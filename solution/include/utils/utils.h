//
// Created by ezh on 30/10/22.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_UTILS_H
#define ASSIGNMENT_IMAGE_ROTATION_UTILS_H

#include <stdint.h>
#include <stdio.h>

#include "print.h"

#define BYTE_SIZE 1

size_t padding(size_t width);
size_t image_with_padding_size(size_t width, size_t height);

#endif //ASSIGNMENT_IMAGE_ROTATION_UTILS_H
