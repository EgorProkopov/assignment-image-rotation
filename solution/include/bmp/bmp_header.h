//
// Created by ezh on 30/10/22.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_HEADER_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_HEADER_H

#include <stdint.h>

#define BMP_HEADER_SIZE sizeof(struct bmp_header)

#define BMP_HEADER_BF_TYPE_1 0x4d42
#define BMP_HEADER_BF_TYPE_2 0x4349
#define BMP_HEADER_BF_TYPE_3 0x5450

#define BMP_CORRECT_HEADER_REVERSED 0
#define BMP_CORRECT_HEADER_PLANES 1
#define BMP_CORRECT_HEADER_BI_SIZE_40 40
#define BMP_CORRECT_HEADER_BI_SIZE_108 108
#define BMP_CORRECT_HEADER_BI_SIZE_124 124
#define BMP_CORRECT_DATA_OFFSET 14

#define BMP_IMAGE_MIN_WIDTH 2
#define BMP_IMAGE_MAX_WIDTH 9999
#define BMP_IMAGE_MIN_HEIGHT 2
#define BMP_IMAGE_MAX_HEIGHT 9999

#define BMP_BITS_COLOR_CHANNELS_3 24
#define BMP_NO_COMPRESSION 0

#define BMP_X_PELS_PER_METER 0
#define BMP_Y_PELS_PER_METER 0
#define BMP_COLORS_IMPORTANT 3

struct __attribute__((packed)) bmp_header
{
    uint16_t bf_type;
    uint32_t bf_file_size;
    uint32_t bf_reserved;
    uint32_t bf_off_bits;
    uint32_t bi_size;
    uint32_t bi_width;
    uint32_t bi_height;
    uint16_t bi_planes;
    uint16_t bi_bit_count;
    uint32_t bi_compression;
    uint32_t bi_size_image;
    uint32_t bi_x_pels_per_meter;
    uint32_t bi_y_pels_per_meter;
    uint32_t bi_clr_used;
    uint32_t bi_clr_important;
};

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_HEADER_H
