//
// Created by ezh on 30/10/22.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_OPEN_CLOSE_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_OPEN_CLOSE_H

#include <stdio.h>

enum bmp_open_status{
    BMP_OPEN_OK = 0,
    BMP_OPEN_ERROR
};

enum bmp_close_status{
    BMP_CLOSE_OK = 0,
    BMP_CLOSE_ERROR
};

struct file_optional{
    enum bmp_open_status status;
    FILE* file_ptr;
};

void print_bmp_open_status(enum bmp_open_status const status);
void print_bmp_close_status(enum bmp_close_status const status);

struct file_optional bmp_open_to_read(char* const path);
struct file_optional bmp_open_to_write(char* const path);
enum bmp_close_status bmp_close(FILE* const file_ptr);

FILE* file_optional_get_file_ptr(struct file_optional const file_optional);
enum bmp_open_status file_optional_get_status(struct file_optional const file_optional);

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_OPEN_CLOSE_H
