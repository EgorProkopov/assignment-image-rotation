//
// Created by ezh on 30/10/22.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include "bmp_open_close.h"
#include "bmp_reader.h"
#include "bmp_writer.h"

struct bmp_work_statuses{
    enum bmp_open_status open_status;
    enum bmp_close_status close_status;
    enum bmp_read_status read_status;
    enum bmp_write_status write_status;
};


#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
