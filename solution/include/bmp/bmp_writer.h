//
// Created by ezh on 30/10/22.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_WRITER_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_WRITER_H

#include "image/image.h"

#include <stdio.h>

//status
enum bmp_write_status{
    BMP_WRITE_OK = 0,
    BMP_WRITE_ERROR
};

void print_bmp_write_status(enum bmp_write_status const status);

//bmp_write function
enum bmp_write_status to_bmp(FILE* out, struct image const* img);

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_WRITER_H
