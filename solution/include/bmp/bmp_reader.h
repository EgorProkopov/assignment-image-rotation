//
// Created by ezh on 30/10/22.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_READER_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_READER_H

#include <stdio.h>
#include <stdint.h>

#include "bmp/bmp_header.h"
#include "image/image.h"


// status
enum bmp_read_status{
    BMP_READ_OK = 0,
    BMP_READ_INVALID_SIGNATURE,

    BMP_READ_INVALID_HEADER,
    BMP_READ_INVALID_HEADER_RESERVED, // BMP_READ_INVALID_HEADER
    BMP_READ_INVALID_HEADER_PLANES, // BMP_READ_INVALID_HEADER
    BMP_READ_INVALID_HEADER_BI_SIZE, // BMP_READ_INVALID_HEADER
    BMP_READ_INVALID_DATA_OFFSET, // BMP_READ_INVALID_HEADER

    BMP_READ_INVALID_WIDTH,
    BMP_READ_INVALID_HEIGHT,
    BMP_READ_INVALID_COLOR_CHANNELS_MODE,
    BMP_READ_INVALID_COMPRESSION,
    BMP_READ_INVALID_FILE_SIZE,

    BMP_READ_INVALID_BITS
};


// print status
void print_bmp_read_status(const enum bmp_read_status status);

// reading bmp
enum bmp_read_status from_bmp(FILE* const in, struct image* image_ptr);

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_READER_H
