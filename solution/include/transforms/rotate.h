//
// Created by ezh on 30/10/22.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATE_H

#include "image/image.h"

struct bmp_work_statuses rotate_bmp(char* const source_path, char* const rotated_path);

#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
