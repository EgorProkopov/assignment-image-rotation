//
// Created by ezh on 30/10/22.
//

#include "bmp/bmp_writer.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "bmp/bmp_header.h"
#include "image/pixel.h"


#include "utils/utils.h"

static struct bmp_header create_bmp_header(size_t width, size_t height){
    struct bmp_header header = {0};
    size_t const image_size = image_with_padding_size(width, height);

    header.bf_type = BMP_HEADER_BF_TYPE_1;
    header.bf_file_size = BMP_HEADER_SIZE + image_size;
    header.bf_reserved = BMP_CORRECT_HEADER_REVERSED;
    header.bf_off_bits = BMP_HEADER_SIZE;
    header.bi_size = BMP_CORRECT_HEADER_BI_SIZE_40;
    header.bi_width = width;
    header.bi_height = height;
    header.bi_planes = BMP_CORRECT_HEADER_PLANES;
    header.bi_bit_count = BMP_BITS_COLOR_CHANNELS_3;
    header.bi_compression = BMP_NO_COMPRESSION;
    header.bi_size_image = image_size;
    header.bi_x_pels_per_meter = BMP_X_PELS_PER_METER;
    header.bi_y_pels_per_meter = BMP_Y_PELS_PER_METER;
    header.bi_clr_used = PIXEL_CHANNELS;
    header.bi_clr_important = BMP_COLORS_IMPORTANT;

    return header;
}

static void write_image(struct image const* img, FILE* out){
    size_t const width = img->width;
    size_t const height = img->height;

    for (size_t i = 0; i < height; i++){
        fseek(out, BMP_HEADER_SIZE + i * PIXEL_CHANNELS * BYTE_SIZE * width + i * padding(width), SEEK_SET);
        fwrite(&img->data[i*width], BYTE_SIZE, PIXEL_CHANNELS * BYTE_SIZE * width, out);
    }
}


enum bmp_write_status to_bmp(FILE* out, struct image const* img){
    enum bmp_write_status status = BMP_WRITE_OK;
    size_t const width = img->width;
    size_t const height = img->height;

    struct bmp_header const header = create_bmp_header(width, height) ;

    fwrite(&header, BYTE_SIZE, BMP_HEADER_SIZE, out);
    write_image(img, out);

    return status;
}

