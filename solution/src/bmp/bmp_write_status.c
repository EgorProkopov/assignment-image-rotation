//
// Created by ezh on 30/10/22.
//

#include "bmp/bmp_writer.h"

#include "utils/print.h"

const char* write_message_string[] = {
        [BMP_WRITE_OK] = ".bmp file wrote successfully",
        [BMP_WRITE_ERROR] = "Invalid writing of .bmp file"
};

void print_bmp_write_status(const enum bmp_write_status status){
    if (status == BMP_WRITE_OK) {
        print_message(write_message_string[status]);
    }
    else{
        print_error_message(write_message_string[status]);
    }
}
