//
// Created by ezh on 30/10/22.
//

#include "bmp/bmp_open_close.h"

#include <stdio.h>

static enum bmp_open_status check_status(FILE* const file_ptr){
    enum bmp_open_status status = BMP_OPEN_OK;
    if (file_ptr == NULL) {
        status = BMP_OPEN_ERROR;
    }
    return status;
}


struct file_optional bmp_open_to_read(char* const path){
    FILE* file_ptr = fopen(path, "r");
    enum bmp_open_status status = check_status(file_ptr);
    return (struct file_optional) {status, file_ptr};
}

struct file_optional bmp_open_to_write(char* const path){
    FILE* file_ptr = fopen(path, "w");
    enum bmp_open_status status = check_status(file_ptr);
    return (struct file_optional) {status, file_ptr};
}

enum bmp_close_status bmp_close(FILE* const file_ptr){
    //int32_t errcode = fclose(file_ptr);
    fclose(file_ptr);

    //TODO: work with error codes
    return BMP_CLOSE_OK;
}

FILE* file_optional_get_file_ptr(struct file_optional const file_optional){
    return file_optional.file_ptr;
}

enum bmp_open_status file_optional_get_status(struct file_optional const file_optional){
    return file_optional.status;
}
