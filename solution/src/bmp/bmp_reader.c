//
// Created by ezh on 30/10/22.
//

#include "bmp/bmp_reader.h"

#include <stdlib.h>

#include "bmp/bmp_open_close.h"
#include "image/pixel.h"

#include "utils/utils.h"

static enum bmp_read_status check_header_size(size_t const header_size){
    if (header_size != BMP_HEADER_SIZE) {
        return BMP_READ_INVALID_HEADER;
    }
    return BMP_READ_OK;
}

static enum bmp_read_status check_header_signature(struct bmp_header const* header_ptr){
    if (header_ptr->bf_type != BMP_HEADER_BF_TYPE_1 && header_ptr->bf_type != BMP_HEADER_BF_TYPE_2 && header_ptr->bf_type != BMP_HEADER_BF_TYPE_3){
        return BMP_READ_INVALID_SIGNATURE;
    }
    return BMP_READ_OK;
}

static enum bmp_read_status check_header_stuff(struct bmp_header const* header_ptr){
    if (header_ptr->bf_reserved != BMP_CORRECT_HEADER_REVERSED ||
        header_ptr->bi_planes != BMP_CORRECT_HEADER_PLANES ||
        (header_ptr->bi_size != BMP_CORRECT_HEADER_BI_SIZE_40 &&
        header_ptr->bi_size != BMP_CORRECT_HEADER_BI_SIZE_108 &&
        header_ptr->bi_size != BMP_CORRECT_HEADER_BI_SIZE_124) ||
        header_ptr->bf_off_bits != BMP_CORRECT_DATA_OFFSET + header_ptr->bi_size){
        return BMP_READ_INVALID_HEADER;
    }
    return BMP_READ_OK;
}

static enum bmp_read_status check_image_size(struct bmp_header const* header_ptr){
    if (!(header_ptr->bi_width >= BMP_IMAGE_MIN_WIDTH && header_ptr->bi_width <= BMP_IMAGE_MAX_WIDTH)){
        return BMP_READ_INVALID_WIDTH;
    } else if (!(header_ptr->bi_height >= BMP_IMAGE_MIN_HEIGHT && header_ptr->bi_height <= BMP_IMAGE_MAX_HEIGHT)){
        return BMP_READ_INVALID_HEIGHT;
    }
    return BMP_READ_OK;
}

static enum bmp_read_status check_image_color_channels_mode(struct bmp_header const* header_ptr){
    if (header_ptr->bi_bit_count != BMP_BITS_COLOR_CHANNELS_3){
        return BMP_READ_INVALID_COLOR_CHANNELS_MODE;
    }
    return BMP_READ_OK;
}

static enum bmp_read_status check_image_compression_mode(struct bmp_header const* header_ptr){
    if (header_ptr->bi_compression != BMP_NO_COMPRESSION){
        return BMP_READ_INVALID_COMPRESSION;
    }
    return BMP_READ_OK;
}

static enum bmp_read_status check_bmp_header(struct bmp_header const* header_ptr, size_t const header_size){
    enum bmp_read_status status = check_header_size(header_size);
    if (status != BMP_READ_OK) return status;

    status = check_header_signature(header_ptr);
    if (status != BMP_READ_OK) return status;

    status = check_header_stuff(header_ptr);
    if (status != BMP_READ_OK) return status;

    status = check_image_size(header_ptr);
    if (status != BMP_READ_OK) return status;

    status = check_image_color_channels_mode(header_ptr);
    if (status != BMP_READ_OK) return status;

    status = check_image_compression_mode(header_ptr);
    return status;
}

enum bmp_read_status from_bmp(FILE* const in, struct image* image_ptr){
    enum bmp_read_status status;

    struct bmp_header header;
    size_t header_size;
    header_size = fread(&header,  BYTE_SIZE,BMP_HEADER_SIZE, in);
    status = check_bmp_header(&header, header_size);

    if (status != BMP_READ_OK){
        return status;
    }

    size_t const image_width = header.bi_width;
    size_t const image_height = header.bi_height;

    init_image(image_ptr, in, image_height, image_width);

    return status;
}
