//
// Created by ezh on 30/10/22.
//

#include "bmp/bmp_reader.h"

#include "utils/print.h"

const char* read_message_string[] = {
        [BMP_READ_OK] = ".bmp file read successfully!\n",
        [BMP_READ_INVALID_SIGNATURE] = "Invalid signature read from .bmp file!\n",
        [BMP_READ_INVALID_HEADER] = "Invalid reading header of .bmp file\n",
        [BMP_READ_INVALID_WIDTH] = "Invalid width of .bmp file\n",
        [BMP_READ_INVALID_HEIGHT] = "Invalid height of .bmp file\n",
        [BMP_READ_INVALID_COLOR_CHANNELS_MODE] = "Invalid color channels mode\n",
        [BMP_READ_INVALID_COMPRESSION] = "Invalid compression mode\n",
        [BMP_READ_INVALID_FILE_SIZE] = "Invalid reading of file size\n",
        [BMP_READ_INVALID_BITS] = "Invalid reading of .bmp file data\n"

};

void print_bmp_read_status(const enum bmp_read_status status){
    if (status == BMP_READ_OK){
        print_message(read_message_string[status]);
    }
    else {
        print_error_message(read_message_string[status]);
    }
}
