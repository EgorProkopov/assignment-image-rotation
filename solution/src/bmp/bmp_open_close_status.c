//
// Created by ezh on 30/10/22.
//

#include "bmp/bmp_open_close.h"

#include "utils/print.h"

void print_bmp_open_status(const enum bmp_open_status status){
    switch (status) {
        case BMP_OPEN_OK: { print_message("File opened successfully!\n"); break; }
        default: {break;}
    }
}

void print_bmp_close_status(const enum bmp_close_status status){

}
