//
// Created by ezh on 30/10/22.
//

#include "transforms/rotate.h"

#include <stdio.h>
#include <stdlib.h>

#include "bmp/bmp.h"
#include "image/pixel.h"


static struct image rotate_image(struct image* img){
    size_t const image_width = img->width;
    size_t const image_height = img->height;

    struct image* new_image = &(struct image){0};

    struct pixel* pixels_data = malloc(image_width * image_height * sizeof(struct pixel));
    new_image->width = image_height;
    new_image->height = image_width;
    new_image->data = pixels_data;

    for (size_t i = 0; i < new_image->height; i++){
        for (size_t j = 0; j < new_image->width; j++){
            struct pixel const pixel = get_pixel(img, i,  new_image->width - j - 1);
            set_pixel(new_image, pixel, j, i);
        }
    }
    return *new_image;
}

struct bmp_work_statuses rotate_bmp(char* const source_path, char* const rotated_path){
    struct bmp_work_statuses bmp_statuses = {BMP_OPEN_OK, BMP_CLOSE_OK, BMP_READ_OK, BMP_WRITE_OK};

    struct file_optional const file_optional = bmp_open_to_read(source_path);

    enum bmp_open_status const bmp_open_status = file_optional_get_status(file_optional);
    if (bmp_open_status != 0){
        bmp_statuses.open_status = bmp_open_status;
        return bmp_statuses;
    }

    // read .bmp file and return status
    FILE* const file_ptr = file_optional_get_file_ptr(file_optional);
    struct image image;
    enum bmp_read_status const bmp_read_status = from_bmp(file_ptr, &image);
    if (bmp_read_status != 0){
        bmp_statuses.read_status = bmp_read_status;
        free_image(&image);
        return bmp_statuses;
    }

    enum bmp_close_status const bmp_close_status = bmp_close(file_ptr);
    if (bmp_close_status != 0){
        bmp_statuses.close_status = bmp_close_status;
        free_image(&image);
        return bmp_statuses;
    }

    //rotate image, args: struct image* image
    struct image new_image = rotate_image(&image);
    free_image(&image);

    //open new .bmp_file and check errors
    struct file_optional const new_file_optional = bmp_open_to_write(rotated_path);

    enum bmp_open_status const bmp_new_open_status = file_optional_get_status(new_file_optional);
    if (bmp_new_open_status != 0){
        bmp_statuses.open_status = bmp_new_open_status;
        free_image(&new_image);
        return bmp_statuses;
    }

    //write new .bmp_file and print status
    FILE* const new_file_ptr = file_optional_get_file_ptr(new_file_optional);
    enum bmp_write_status const bmp_new_write_status = to_bmp(new_file_ptr, &new_image);
    if (bmp_new_write_status != 0){
        bmp_statuses.write_status = bmp_new_write_status;
        free_image(&new_image);
        return bmp_statuses;
    }
    enum bmp_close_status const bmp_new_close_status = bmp_close(new_file_ptr);
    if (bmp_new_close_status != 0){
        bmp_statuses.close_status = bmp_new_close_status;
        free_image(&new_image);
        return bmp_statuses;
    }
    free_image(&new_image);

    return bmp_statuses;
}
