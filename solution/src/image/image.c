//
// Created by ezh on 30/10/22.
//

#include "image/image.h"

#include <stdlib.h>

#include "bmp/bmp_header.h"
#include "image/pixel.h"

#include "utils/utils.h"

void set_pixel(struct image* image, struct pixel const pixel, size_t x, size_t y){
    image->data[y*image->width + x] = pixel;
}

struct pixel get_pixel(struct image const* image, size_t x, size_t y){
    return image->data[y*image->width + x];
}

void init_image(struct image* image, FILE* const in, size_t const height, size_t const width){
    struct pixel* pixels_data = malloc(height * width * sizeof(struct pixel));

    for (size_t i = 0; i < height; i++){
        fseek(in, BMP_HEADER_SIZE + i * PIXEL_CHANNELS * BYTE_SIZE * width + i * padding(width), SEEK_SET);
        fread(&pixels_data[i*width], BYTE_SIZE, PIXEL_CHANNELS * BYTE_SIZE * width, in);
    }

    image->width = width;
    image->height = height;
    image->data = pixels_data;
}

void free_image(struct image* image){
    struct pixel* pixel_data= image->data;
    free(pixel_data);
}
