//
// Created by ezh on 01/11/22.
//

#include "utils/utils.h"

#include "image/pixel.h"

size_t padding(size_t width){
    return (-PIXEL_CHANNELS * width) % 4;
}

size_t image_with_padding_size(size_t width, size_t height){
    return height * (width * PIXEL_CHANNELS + padding(width));
}
