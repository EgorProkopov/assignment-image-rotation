//
// Created by ezh on 30/10/22.
//

#include <stdio.h>

void print_message(char* message){
    fprintf(stdout,"%s", message);
}

void print_error_message(char* error_message){
    fprintf(stderr, "%s", error_message);
}
