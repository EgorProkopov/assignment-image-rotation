#include "bmp/bmp.h"
#include "transforms/transforms.h"

#include "utils/utils.h"

int main(int argc, char **argv) {
    if (argc != 3){
        print_error_message("Incorrect number of arguments!");
        return 1;
    }
    char* const source_path = argv[1];
    char* const rotated_path = argv[2];

    struct bmp_work_statuses const statuses = rotate_bmp(source_path, rotated_path);

    if (statuses.open_status != 0) {
        return statuses.open_status;
    } else if (statuses.close_status != 0) {
        return statuses.close_status;
    } else if (statuses.read_status != 0) {
        return statuses.read_status;
    } else if (statuses.write_status != 0) {
        return statuses.write_status;
    }

    return 0;
}
